# Adreno GPU acceleration on Termux-x11

## Setup

1 - Install termux and termux-x11 from github CI artifacts

2 - Install git in termux : `pkg add git`

3 - Clone this repo in termux : `git clone https://gitlab.com/Azkali/adreno_termux` 

4 - Run `cd adreno_termux; chmod +x setup.sh; ./setup.sh`
